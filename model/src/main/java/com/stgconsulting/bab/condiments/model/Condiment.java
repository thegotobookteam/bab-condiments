package com.stgconsulting.bab.condiments.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Bun model.
 */
@Data
@NoArgsConstructor
@Entity
public class Condiment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(value = "id", required = true, dataType = "Long", example = "123")
    private Long id;

    @ApiModelProperty(value = "name", required = true, dataType = "String", example = "Deli Mustard")
    private String name;

    @ApiModelProperty(value = "description", required = true, dataType = "String", example = "Toasted sandwich bread was the original burger bun. Denser-textured breads, like the ones from Pepperidge Farm, hold up well to patties thick and thin.")
    private String description;

    @ApiModelProperty(value = "image_path", required = true, dataType = "String", example = "DELI-MUSTARD.png")
    @JsonProperty(value = "image_path")
    private String imagePath;
}
