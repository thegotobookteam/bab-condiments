package com.stgconsulting.bab.condiments.model.constants;

import static com.stgconsulting.bab.condiments.model.constants.Headers.CONDIMENT_ID;

/**
 * Resource constants.
 * <p>
 * Note:
 * By convention, such fields have names consisting of capital letters, with words separated by underscores.
 * It is critical that these fields contain either primitive values or references to immutable objects.
 */
public class Resources {

    public static final String REST_PREFIX = "/rest";
    public static final String BASE_RESOURCE = "/condiments";

    public static final String API_CONDIMENTS_ALL_URI = BASE_RESOURCE;
    public static final String API_CONDIMENTS_ALL_DESC = "Retrieve list of all hamburger condiments.";

    public static final String API_CONDIMENT_BY_ID_URI = BASE_RESOURCE + "/{" + CONDIMENT_ID + "}";
    public static final String API_CONDIMENT_BY_ID_DESC = "Retrieve details of a hamburger condiment.";

    public static final String API_CONDIMENT_CREATE_URI = BASE_RESOURCE;
    public static final String API_CONDIMENT_CREATE_DESC = "Create a hamburger condiment.";

    public static final String API_CONDIMENT_UPDATE_URI = BASE_RESOURCE;
    public static final String API_CONDIMENT_UPDATE_DESC = "Update a hamburger condiment information.";

    public static final String API_CONDIMENT_DELETE_BY_ID_URI = BASE_RESOURCE + "/{" + CONDIMENT_ID + "}";
    public static final String API_CONDIMENT_DELETE_BY_ID_DESC = "Delete a hamburger condiment.";

    public static final String API_CONDIMENT_IMAGE_BY_ID_URI = BASE_RESOURCE + "/{" + CONDIMENT_ID + "}/image";
    public static final String API_CONDIMENT_IMAGE_BY_ID_DESC = "Retrieve condiment image.";

}
