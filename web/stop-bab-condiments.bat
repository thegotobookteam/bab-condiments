@echo off
rem -------------------------------------------------------------------
rem Stopping Build-A-Burger micro-service
rem -------------------------------------------------------------------
echo Stopping existing Build-A-Burger micro-service...
docker stop bab-condiments

