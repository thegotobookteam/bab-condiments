package com.stgconsulting.bab.condiments.web.service;

import com.stgconsulting.bab.condiments.model.Condiment;
import com.stgconsulting.bab.condiments.web.dao.CondimentsDao;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Condiment service.
 */
@Service
public class CondimentService {

    private CondimentsDao condimentsDao;

    public CondimentService(CondimentsDao condimentsDao) {
        this.condimentsDao = condimentsDao;
    }

    public Condiment findOne(Long pId) {
        return condimentsDao.findOne(pId);
    }

    public List<Condiment> findAll() {
        return condimentsDao.findAll();
    }

    public Integer create(Condiment pCondiment) {
        return condimentsDao.create(pCondiment);
    }

    public Integer update(Condiment pCondiment) {
        return condimentsDao.update(pCondiment);
    }

    public Integer deleteById(Long pId) {
        return condimentsDao.delete(pId);
    }

}
