package com.stgconsulting.bab.condiments.web.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import com.stgconsulting.bab.condiments.model.Condiment;
import com.stgconsulting.bab.condiments.web.service.CondimentService;
import com.stgconsulting.bab.condiments.web.util.RestPreconditions;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static com.stgconsulting.bab.condiments.model.constants.Headers.CONDIMENT_ID;
import static com.stgconsulting.bab.condiments.model.constants.Resources.*;

@Slf4j
@RestController
@RequestMapping(REST_PREFIX)
public class Condiments {

    private CondimentService condimentService;
    private ObjectMapper objectMapper;

    /**
     * DI Constructor
     */
    public Condiments(CondimentService pCondimentService) {
        condimentService = pCondimentService;
        objectMapper = new ObjectMapper();
    }

    @GetMapping(value = API_CONDIMENTS_ALL_URI, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Condiment> findAll() {
        log.info("findAll() :: Enter/Exit");
        return condimentService.findAll();
    }

    @GetMapping(value = API_CONDIMENT_BY_ID_URI, produces = MediaType.APPLICATION_JSON_VALUE)
    public Condiment findById(@Valid @PathVariable(CONDIMENT_ID) Long pCondimentId) {
        log.info("findById() :: Enter/Exit");
        return RestPreconditions.checkFound(condimentService.findOne(pCondimentId));
    }

    @PostMapping(value = API_CONDIMENT_CREATE_URI, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Integer create(@RequestParam(required = true, value = "condimentJson") String pCondimentJson) {
        log.info("create() :: Enter/Exit");
        Condiment condiment = null;
        try {
            condiment = objectMapper.readValue(pCondimentJson, Condiment.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        Preconditions.checkNotNull(condiment);
        return condimentService.create(condiment);
    }

    @PutMapping(value = API_CONDIMENT_UPDATE_URI, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody Condiment pCondiment) {
        log.info("update() :: Enter");
        Preconditions.checkNotNull(pCondiment);
        RestPreconditions.checkFound(condimentService.findOne(pCondiment.getId()));
        condimentService.update(pCondiment);
        log.info("update() :: Exit");
    }

    @DeleteMapping(value = API_CONDIMENT_DELETE_BY_ID_URI, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void deleteById(@PathVariable(CONDIMENT_ID) Long pId) {
        log.info("deleteById() :: Enter");
        condimentService.deleteById(pId);
        log.info("deleteById() :: Exit");
    }

    @GetMapping(value = API_CONDIMENT_IMAGE_BY_ID_URI, produces = MediaType.IMAGE_JPEG_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    byte[] getImage(@Valid @PathVariable(CONDIMENT_ID) Long pCondimentId) throws IOException {
        log.info("getImage() :: Enter");
        Condiment condiment = condimentService.findOne(pCondimentId);
        InputStream in = getClass().getResourceAsStream("/images/" + condiment.getImagePath());
        log.info("getImage() :: Exit");
        return IOUtils.toByteArray(in);
    }
}
