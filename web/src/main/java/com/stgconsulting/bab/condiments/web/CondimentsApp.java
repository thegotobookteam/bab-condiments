package com.stgconsulting.bab.condiments.web;

import lombok.extern.java.Log;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.PropertySource;

@Log
@EnableDiscoveryClient
@SpringBootApplication
@PropertySource( "classpath:build.properties" )
public class CondimentsApp {
    public static void main( String[] args ) {
        log.info( "main() :: Enter");

        SpringApplication.run(CondimentsApp.class, args);

        log.info( "main() :: Exit" );
    }
}
