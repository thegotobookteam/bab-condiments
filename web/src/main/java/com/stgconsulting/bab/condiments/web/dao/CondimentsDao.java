package com.stgconsulting.bab.condiments.web.dao;

import com.stgconsulting.bab.condiments.model.Condiment;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

/**
 * Condiments DAO or Data Access Object.
 */
@Repository
@NoArgsConstructor
public class CondimentsDao {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDatasource(final DataSource pDataSource) {
        this.jdbcTemplate = new JdbcTemplate(pDataSource);
        final CustomSQLErrorCodeTranslator customSQLErrorCodeTranslator = new CustomSQLErrorCodeTranslator();
        jdbcTemplate.setExceptionTranslator(customSQLErrorCodeTranslator);
    }

    public int getCount() {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM CONDIMENTS", Integer.class);
    }

    public List<Condiment> findAll() {
        return jdbcTemplate.query("SELECT * FROM CONDIMENTS", new CondimentsRowMapper());
    }

    public Condiment findOne(final long pCondimentId) {
        final String query = "SELECT * FROM CONDIMENTS WHERE ID = ?";
        return jdbcTemplate.queryForObject(query, new Object[]{pCondimentId}, new CondimentsRowMapper());
    }

    public Integer create(final Condiment pCondiment) {
        return jdbcTemplate.update("INSERT INTO CONDIMENTS VALUES (?, ?, ?, ?)",
                pCondiment.getId(), pCondiment.getName(), pCondiment.getDescription(), pCondiment.getImagePath());
    }

    public Integer update(final Condiment pCondiment) {
        return jdbcTemplate.update("UPDATE BUNS SET name = ?, description = ?, file_image = ? WHERE ID = ?",
                pCondiment.getName(), pCondiment.getDescription(), pCondiment.getImagePath(), pCondiment.getId());
    }

    public Integer delete(final Long pId) {
        return jdbcTemplate.update("DELETE FROM CONDIMENTS WHERE ID = ?", pId);
    }
}
