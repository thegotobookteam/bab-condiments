package com.stgconsulting.bab.condiments.web.dao;

import com.stgconsulting.bab.condiments.model.Condiment;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Condiment Row Mapper class.
 */
public class CondimentsRowMapper implements RowMapper<Condiment> {

    @Override
    public Condiment mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        Condiment bun = new Condiment();

        bun.setId(resultSet.getLong("id"));
        bun.setName(resultSet.getString("name"));
        bun.setDescription(resultSet.getString("description"));
        bun.setImagePath(resultSet.getString("image_path"));
        return bun;
    }
}
