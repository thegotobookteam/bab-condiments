@echo off
rem -------------------------------------------------------------------
rem Execute Build-A-Burger micro-service
rem -------------------------------------------------------------------
echo Executing Build-A-Burger micro-service...
docker run --rm -p 8003 --network host --name bab-condiments bab-buns-condiments:1.LOCAL
