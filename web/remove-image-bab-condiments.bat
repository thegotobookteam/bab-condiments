@echo off
rem -------------------------------------------------------------------
rem Remove Build-A-Burger micro-service Docker image
rem -------------------------------------------------------------------
echo Remove Build-A-Burger micro-service Docker image...
docker rmi -f bab-condiments:1.LOCAL
